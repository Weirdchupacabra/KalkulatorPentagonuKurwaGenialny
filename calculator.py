print("Calculator")
while True:
    try:
        expression = input("Enter a calculation or type 'exit' to quit: ")
        if expression.lower() == 'exit':
            break
        result = eval(expression)
        print("Result: ", result)
    except Exception as e:
        print("Invalid input, please try again.")
